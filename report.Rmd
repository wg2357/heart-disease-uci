---
title: "Causal Inference using UCI Heart Disease Data"
author: "Wren"
date: "March 31, 2020"
output:
     html_document:
      code_folding: hide
      toc: yes
      toc_float: yes
abstract: "The following causal analysis estimates treatment effects of factors associated with heart disease. We use logistic regression and LASSO logistic regression on a subset of features chosen with the PC algorithm. We find significant change in probability of heart disease depending on chest pain type, presence of exercise-induced angina, results of thallium stress test, and results of fluoroscopy test. Finally, we show the causal logistic models have comparable test accuracy to random forest and gradient boosting models."
editor_options: 
  chunk_output_type: console
---

```{r setup, include=TRUE, message=FALSE}
library(knitr)
library(magrittr)
library(GGally)
library(glmnet)
library(randomForest)
library(xgboost)
library(pcalg)
library(tidyverse)
library(kableExtra)
library(gridExtra)
opts_chunk$set(cache=TRUE, autodep=TRUE, cache.comments=FALSE,
               message=FALSE, warning=FALSE)
```

# Introduction

Heart disease is the leading cause of death in the United States according to the CDC, comprising approximately 23% of all deaths^[https://www.cdc.gov/nchs/data/nvsr/nvsr68/nvsr68_06-508.pdf]. Understanding the causes of heart disease and associated risk factors is an important step in reducing the harm caused by the disease. The following analysis investigates several models in an attempt to quantify the causal relationships between heart disease and associated symptoms.

The data^[https://archive.ics.uci.edu/ml/datasets/Heart+Disease] come from the UCI Machine Learning Repository^[https://archive.ics.uci.edu/ml/index.php] (downloaded from Kaggle^[https://www.kaggle.com/ronitf/heart-disease-uci]). There are 14 recorded variables including the presence or absence of heart disease. 

We begin with a graphical exploration of the measured variables. We then use logistic regression and LASSO logistic regression to get a baseline model with which we can compare subsequent causal models. We use the PC algorithm for causal discovery, then estimate causal effects with logistic regression and logistic LASSO. Finally, we use 5-fold cross-validation to estimate test accuracy of our causal models and compare them to popular machine-learning models. 

# Exploratory Data Analysis

```{r}
dt_original <- read_csv("heart.csv")

dt_original %>% 
  # remove adjacent duplicate entry, which is possibly an entry error
  magrittr::extract(-165, ) %>% 
  # recode indicators as factors with labels
  mutate(sex=ifelse(sex == 1, "Male", "Not Male") %>% 
           as_factor(), 
         cp=case_when(cp == 0 ~ "Asymptomatic",
                      cp == 1 ~ "Atypical Angina",
                      cp == 2 ~ "Non-Anginal Pain",
                      cp == 3 ~ "Typical Angina") %>% 
           as_factor() %>% 
           fct_relevel("Asymptomatic", after=0),
         fbs=ifelse(fbs==1, "> 120 mg/dl", "<= 120 mg/dl") %>% 
           as_factor() %>% 
           fct_relevel("<= 120 mg/dl", after=0),
         restecg=case_when(restecg == 0 ~ "Normal",
                           restecg == 1 ~ "Having ST-T Wave Abnormality",
                           restecg == 2 ~ "Possible or Definite\nLeft Ventricular Hypertrophy") %>% 
           as_factor(),
         exang=ifelse(exang==1, "Induced", "Not Induced") %>% 
           as_factor(),
         slope=case_when(slope == 0 ~ "Downsloping", 
                         slope == 1 ~ "Flat", 
                         slope == 2 ~ "Upsloping") %>% 
           as_factor() %>% 
           fct_relevel("Flat", after=0), 
         thal=case_when(thal == 1 ~ "Normal", 
                        thal == 2 ~ "Fixed Defect",
                        thal == 3 ~ "Reversable Defect") %>% 
           as_factor()) %>% 
  # NAs in original dataset according to https://www.kaggle.com/ronitf/heart-disease-uci/discussion/105877
  filter(ca!=4) %>% 
  # two thal values are missing, which 
  drop_na() ->
  dt
```

We have the following variables (adapted from Rob Harrand's kernel on Kaggle^[https://www.kaggle.com/tentotheminus9/what-causes-heart-disease-explaining-the-model], explanations additionally borrowed from IntiPic^[https://www.kaggle.com/ronitf/heart-disease-uci/discussion/105877]):

+ `age`
+ `sex`
+ `cp`: The chest pain experienced 
+ `trestbps`: Resting blood pressure (mm Hg on admission to the hospital)
+ `chol`: Cholesterol measurement in mg/dl
+ `fbs`: Fasting blood sugar 
+ `restecg`: Resting electrocardiographic measurement (0 = normal, 1 = having ST-T wave abnormality, 2 = showing probable or definite left ventricular hypertrophy by Estes' criteria)
+ `thalach`: Maximum heart rate achieved
+ `exang`: Exercise induced angina
+ `oldpeak`: ST depression induced by exercise relative to rest ('ST' relates to positions on the ECG plot)
+ `slope`: the slope of the peak exercise ST segment
+ `ca`: The number of major vessels colored by fluorosopy
+ `thal`: Results of thallium stress test
+ `target`: Presence of heart disease

## Univariate EDA

### Demographics 

```{r}
demo_p1 <- 
  dt %>% 
  ggplot(aes(x=age)) + 
  geom_histogram(binwidth=2, color="black")

demo_p2 <- 
  dt %>% 
  ggplot(aes(x=sex)) + 
  geom_bar()

grid.arrange(demo_p1, demo_p2, nrow=1)
```

### Medical Variables

```{r, fig.height=24}
med_plot_1 <-
  dt %>% 
  ggplot(aes(x=cp)) + 
  geom_bar() + 
  theme(axis.text.x=element_text(hjust=1, angle=45))

med_plot_2 <-
  dt %>% 
  ggplot(aes(x=trestbps)) + 
  geom_histogram(binwidth=10, color="black")

med_plot_3 <-
  dt %>% 
  ggplot(aes(x=chol)) + 
  geom_histogram(binwidth=25, color="black")

med_plot_4 <-
  dt %>% 
  ggplot(aes(x=fbs)) + 
  geom_bar()

med_plot_5 <-
  dt %>% 
  ggplot(aes(x=restecg)) + 
  geom_bar() + 
  theme(axis.text.x=element_text(hjust=1, angle=45))

med_plot_6 <-
  dt %>% 
  ggplot(aes(x=thalach)) + 
  geom_histogram(binwidth=10, color="black")

med_plot_7 <-
  dt %>% 
  ggplot(aes(x=exang)) + 
  geom_bar()

med_plot_8 <-
  dt %>% 
  ggplot(aes(x=oldpeak)) + 
  geom_histogram(binwidth=0.5, color="black")

med_plot_9 <-
  dt %>% 
  ggplot(aes(x=slope)) + 
  geom_bar()

med_plot_10 <-
  dt %>% 
  ggplot(aes(x=ca)) + 
  geom_bar()

med_plot_11 <-
  dt %>% 
  ggplot(aes(x=thal)) + 
  geom_bar()

grid.arrange(
  med_plot_1,
  med_plot_2,
  med_plot_3,
  med_plot_4,
  med_plot_5,
  med_plot_6,
  med_plot_7,
  med_plot_8,
  med_plot_9,
  med_plot_10,
  med_plot_11,
  ncol=2
)
```

The individuals in our data are generally middle-aged and about two-thirds male. Several categorical variables have one category in which very few observations fall. For example, `restecg` has a category, "Possible or Definite Left Ventricular Hypertrophy", containing only 4 individuals, and `thal` has 18 individuals identified as "Normal". 

## Bivariate 

```{r, fig.height=14, fig.width=14}
dt %>% 
  select_if(function(x){!is.factor(x)}) %>% 
  mutate(ca=as_factor(ca),
         target=as.factor(target)) %>% 
  ggpairs(aes(color=target),
          lower=list(continuous=wrap(ggally_points, 
                                     cex=0.75,
                                     alpha=0.5)))
```

There appears to be relatively low correlation between the continuous variables in the data. The distribution of age is wider for those with heart disease than those without. The distribution of `thalach`, or heart rate, for those with heart disease is higher and more concentrated than for those without. Notably, those with heart disease overwhelmingly have a value of 0 for `ca`. 

```{r, fig.height=10, fig.width=10}
map(names(select_if(dt, is.factor)), function(x){
  x <- sym(x)
  ggplot(dt, aes(x=!!x, fill=as.factor(target))) + 
    geom_bar(position="fill") + 
    labs(y="Proportion") + 
    coord_flip() + 
    labs(fill="target")
}) %>% 
  grid.arrange(grobs=., ncol=2)
```

We see a higher proportion of individuals with chest pain having heart disease. Similarly, a higher proportion of individuals with exercise induced angina have heart disease, as do individuals with "fixed defect" values of `thal`. Interestingly, fewer than half of the male individuals in the data have heart disease, while about 75% of the non-male individuals do have heart disease.

# Modeling
## Associative Models
### Logistic Regression

We begin with a straightforward logistic regression including all variables. This will serve as a baseline for comparing later models to see if or how coefficient estimates and accuracy changes. We use a parametric bootstrap to estimate confidence intervals.

```{r}
lgr_all <- glm(target~., family="binomial", data=dt)
```

```{r, warning=FALSE}
# bootstrap
set.seed(42)

boot_sample_lgr <- replicate(1000, 
                             rbernoulli(nrow(dt), p=lgr_all$fitted.values) %>% 
                               as.numeric())
boot_est_lgr <- 
  map_df(1:1000, 
         function(b, bs_f, dt_f){
           boot_data <- 
             select(dt_f, -target) %>% 
             mutate(target=bs_f[, b])
           boot_model <- 
             glm(target~., family="binomial", data=boot_data)
           boot_model$coefficients %>% 
             t() %>% 
             as_tibble() %>% 
             return()
         }, 
         boot_sample_lgr,
         dt)
```

```{r}
ci_lower_lgr <- 2*lgr_all$coefficients - apply(boot_est_lgr, 2, quantile, 0.975)
ci_upper_lgr <- 2*lgr_all$coefficients - apply(boot_est_lgr, 2, quantile, 0.025)

lgr_coef <- 
  tibble(var=names(lgr_all$coefficients),
         lower=ci_lower_lgr,
         est=lgr_all$coefficients,
         upper=ci_upper_lgr) %>% 
  mutate(var=str_replace(var, "([a-z]+)([A-Z])", "\\1: \\2"))

linear_coef_plot <- 
  ggplot() +
  geom_point(data=lgr_coef, aes(x=var, y=est)) + 
  geom_segment(data=lgr_coef, aes(x=var, xend=var, y=lower, yend=upper)) + 
  geom_hline(yintercept=0, color="red", linetype="dashed") +
  coord_flip() + 
  labs(title="Logistic Regression Coefficient Estimates",
       subtitle="Bootstrap 95% Confidence Intervals",
       x="Variable",
       y="Coeffient Estimate (log-odds)") 

linear_coef_plot
```

Since only 4 individuals had Left Ventricular Hypertrophy, our estimate of this coefficient is wildly uncertain. The intercept also covers about 99% of the probability space. For better visualization, we plot just the other variables.

```{r}
linear_zoom_plot <- 
  ggplot() +
  geom_point(data=slice(lgr_coef, -1, -11), 
             aes(x=var, y=est)) + 
  geom_segment(data=slice(lgr_coef, -1, -11), 
               aes(x=var, xend=var, y=lower, yend=upper)) + 
  geom_hline(yintercept=0, color="red", linetype="dashed") +
  coord_flip() + 
  labs(title="Logistic Regression Coefficient Estimates",
       subtitle="Intercept and Ventricular Hypertrophy Removed",
       x="Variable",
       y="Coeffient Estimate (log-odds)") 

linear_zoom_plot
```

### LASSO Logistic Regression

```{r}
odds_to_prob <- function(x){1/(1+exp(-x))}

set.seed(1235)
dt_scaled <- 
  dt %>% 
  select(-target) %>% 
  mutate_if(is.numeric, function(x){scale(x, center=T, scale=T)}) %>%
  bind_cols(., select(dt, target))

lasso_pv <- model.matrix(target~., data=dt_scaled)[, -1]

cv_lasso <- 
  cv.glmnet(y=dt$target, x=lasso_pv,
            alpha=1, family="binomial") 

lasso_mdl <- 
  glmnet(y=dt$target %>% as.numeric(), x=lasso_pv,
         alpha=1, family="binomial", lambda=cv_lasso$lambda.1se)

lasso_mdl_fit_prob <- 
  predict.glmnet(lasso_mdl, newx=lasso_pv) %>% 
  magrittr::extract(,1) %>% 
  odds_to_prob()
```

```{r}
set.seed(89)

boot_sample_lasso <- 
  replicate(1000, 
            rbernoulli(nrow(dt), p=lasso_mdl_fit_prob) %>% 
              as.numeric())

boot_est_lasso <- 
  map_df(1:1000, 
         function(b, bs_f, dt_f){
           boot_data <- 
             select(dt_f, -target) %>% 
             mutate(target=bs_f[, b]) %>% 
             model.matrix(target~., data=.) %>% 
             magrittr::extract(, -1)
           boot_model <- 
             glmnet(y=bs_f[, b], x=boot_data,
                    alpha=1, family="binomial", lambda=cv_lasso$lambda.1se)
           boot_model$beta %>%
             as.matrix() %>% 
             t() %>% 
             as_tibble() %>% 
             return()
         }, 
         boot_sample_lasso,
         dt)
```

```{r}
lasso_coef <- magrittr::extract(lasso_mdl$beta, , 1)

ci_lower_lasso <- 2*lasso_coef - apply(boot_est_lasso, 2, quantile, 0.975)
ci_upper_lasso <- 2*lasso_coef - apply(boot_est_lasso, 2, quantile, 0.025)

lasso_tib <- 
  tibble(var=names(lasso_coef),
         lower=ci_lower_lasso,
         est=lasso_coef, 
         upper=ci_upper_lasso) %>% 
  mutate(var=str_replace(var, "([a-z]+)([A-Z])", "\\1: \\2"))

lasso_coef_plot <- 
  ggplot() +
  geom_point(data=lasso_tib, aes(x=var, y=est)) + 
  geom_segment(data=lasso_tib, aes(x=var, xend=var, y=lower, yend=upper)) + 
  geom_hline(yintercept=0, color="red", linetype="dashed") +
  coord_flip() + 
  labs(title="Logistic LASSO Coefficient Estimates",
       subtitle="Bootstrap 95% Confidence Intervals",
       x="Variable",
       y="Coeffient Estimate (log-odds)") 

lasso_coef_plot
```

As expected, many coefficient estimates were shrunk to 0. `thal`, `slope`, `sex`, `exang`, `cp`, and `ca` remain as those with the most predictive power.

### Comparison of LASSO and Logistic Regression Coefficient Estimates

For easier comparison, we can plot the coefficient estimates on the same scale.

```{r, fig.height=10}
grid.arrange(linear_zoom_plot + scale_y_continuous(limits=c(-3.5, 3.5)),
             lasso_coef_plot + scale_y_continuous(limits=c(-3.5, 3.5)))
```

## Causal Models

To estimate causal effects, we need to propose some structure of relationships between the variables we are working with. Because we don't have much domain knowledge of heart disease, we choose to use the PC algorithm for causal discovery, which tests for conditional independence between pairs of variables and estimates plausible structure in the form of a directed acyclic graph. However, we first need a measurement of similarity between our set of variables.

### Kernel Alignment

Handhayani, Cussens 2019^[https://arxiv.org/pdf/1910.03055v1.pdf] proposes using kernel alignment as a substitute for the correlation matrix usually used in the PC algorithm (package `pcalg`^[https://cran.r-project.org/web/packages/pcalg/index.html]). This is appropriate because our data contain both categorical and continuous variables. 

```{r}
# Here are functions used to calculate the kernel alignment matrix of our data

# Calculate RBF Kernel given two values and a parameter sigma
rbf_kern <- function(i, j, sig){
  if (sig <= 0){
    stop("\"sig\" must be greater than 0")
  }
  exp(-(i - j)^2/(2*sig^2)) %>% 
    return()
}

# Calculate RBF kernel matrix given a vector x and parameter sigma
rbf_mat <- function(x, sig){
  g <- expand_grid(r=x, c=x)
  map2_dbl(g$r, g$c, rbf_kern, sig) %>% 
    matrix(nrow=length(x))
}

# Helper function for categorical kernel 
h_theta <- function(z, theta){
  (1-z^theta)^(1/theta)
}

# Calculate categorical kernel matrix given a vector x and parameter theta
cat_mat <- function(x, theta){
  if (theta <= 0){
    stop("\"theta\" must be greater than 0")
  }
  probs <-
    enframe(table(x) / length(x)) %>% 
    mutate(value=as.numeric(value))
  g <- expand_grid(r=x, c=x)
  map2_dbl(g$r, g$c, function(i, j, theta){
    if (i!=j){
      return(0)
    } else {
      p <- 
        filter(probs, name==i) %>% 
        magrittr::extract2("value")
      h_theta(p, theta) %>% 
        return()
    }
  }, theta) %>% 
    matrix(nrow=length(x))
}

# Calculate alignments given two kernels
kernel_alignment <- function(ki, kj){
  (sum(ki * kj)/sqrt(sum(ki^2) * sum(kj^2))) %>% 
    return()
}

# Calculate kernel alignment matrix given a list of kernel matrices, 1/variable
ka_mat <- function(kernel_matrix_list){
  g <- 
    expand_grid(x=1:length(kernel_matrix_list),
                y=1:length(kernel_matrix_list))
  map2(g$x, g$y, function(i, j){
    kernel_alignment(kernel_matrix_list[[i]], kernel_matrix_list[[j]])
  }) %>% 
    matrix(nrow=length(kernel_matrix_list))
}

# Given Data, parameters theta and sigma, calculate kernel alignment matrix
# for use in PC or FCI algorithm
kernel_alignment_matrix <- function(data_f, sigma, theta){
  map(names(data_f), function(name){
    x <- magrittr::extract2(data_f, name)
    if (is.numeric(x)){
      rbf_mat(x, sigma) %>% 
        scale(., center=TRUE, scale=FALSE) %>% 
        return()
    } else {
      cat_mat(x, theta) %>% 
        scale(., center=TRUE, scale=FALSE) %>% 
        return()
    }
  }) %>% 
    ka_mat() %>% 
    as.double() %>% 
    matrix(nrow=ncol(data_f)) %>% 
    return()
}
```

The kernel functions used to calculate kernel alignment require a choice of parameters $\sigma$ and $\theta$. To determine appropriate values, we can plot several choices to get a sense of how the parameter choice may ultimately affect the alignment matrix.

```{r}
x <- seq(from=-6, to=6, by=0.1)

sigs <- c(0.001, 0.01, 0.05, 0.1, 0.25, 0.5, 0.75, 1)

map_dfc(sigs, rbf_kern, i=x, j=0) %>% 
  rename_all(function(x){x=sigs}) %>% 
  bind_cols(dist=x, .) %>% 
  pivot_longer(-dist,
               names_to="sigma") %>% 
  ggplot(aes(x=dist, y=value, color=sigma)) + 
  geom_line() + 
  scale_x_continuous(breaks=seq(-6, 6, by=2)) + 
  labs(title="RBF Kernel",
       x="Distance",
       y="Kernel Output",
       color="\u03C3")

p <- seq(from=0.05, to=0.95, by=0.05)

thetas <- c(0.25, 0.5, 0.75, 1, 1.5, 2)

map_dfc(thetas, h_theta, z=p) %>% 
  rename_all(function(x){x=thetas}) %>% 
  bind_cols(p=p, .) %>% 
  pivot_longer(-p, 
               names_to="theta") %>%  
  ggplot(aes(x=p, y=value, color=theta)) + 
  geom_path() + 
  geom_point() + 
  labs(title="Categorical Kernel",
       subtitle="Kernel output=0 when 2 observations in different categories",
       x="Proportion of Observations with Given Category",
       y="Kernel Output",
       color="\u03B8")
```

We see the RBF kernel has a Gaussian shape, and the parameter $\sigma$ determines how slowly or quickly a distance shrinks to 0. The choice of $\theta$ in the categorical kernel indicates how strongly to weight categories with few observations. This kernel gives little weight to common categories, which makes sense intuitively. If almost all observations fall in a single category, we are not gaining much useful information from that.

### Causal Structure

```{r}
# The following code generated the alignment-search-results.rds

sigmas <- c(0.01, 0.05, 0.1, 0.25, 0.5)
thetas <- c(0.5, 0.75, 1, 1.5)
params <- expand_grid(sigmas=sigmas, thetas=thetas)

# dt_scaled <- 
#   dt %>% 
#   mutate(target=as.factor(target)) %>% 
#   mutate_if(is.numeric, scale, center=TRUE, scale=TRUE)
# 
# alignment_search <- 
#   map2(params$sigmas, params$thetas, kernel_alignment_matrix, data_f=dt_scaled)
# 
# saveRDS(alignment_search, "alignment-search-results.rds")
```

We calculated alignment matrices using $\sigma$ = `r sigmas`, and $\theta$ = `r thetas`, and used these matrices as inputs for the PC algorithm. There were essentially two different graphs estimated. 

```{r, fig.height=12, fig.width=12}
asr <- readRDS("alignment-search-results.rds")

walk(5:6, function(x){
  pc(list(C=asr[[x]],
          n=nrow(dt)), 
     indepTest=gaussCItest, 
     p=ncol(dt), 
     alpha=0.05) %>% 
    plot(., 
         main=str_c("sigma=", params[x, 1], ", ",
                    "theta=", params[x, 2]), 
         labels=colnames(dt))
}) 
```

The structure of these graphs echos the results of the LASSO model: the variables not identified to be causally linked to `target` were almost all shrunk to 0, with the exception of `oldpeak` and `slope`. This suggests that using the PC algorithm with kernel alignment is not wildly off-base.

Fortunately, both graphs indicate that a model including `cp`, `exang`, `ca`, `thal`, and `target` will be sufficient for estimating causal effects.

```{r}
dt_causal <- 
  select(dt, cp, exang, ca, thal, target)

dt_causal_scaled <- 
  select(dt_scaled, cp, exang, ca, thal, target)

lgr_causal <- 
  glm(target~., family="binomial", 
      data=dt_causal)
```

```{r, warning=FALSE}
# bootstrap the causal model
set.seed(643)

boot_sample_lgr_causal <- 
  replicate(1000, 
            rbernoulli(nrow(dt), 
                       p=lgr_causal$fitted.values) %>% 
              as.numeric())

boot_est_lgr_causal <- 
  map_df(1:1000, 
         function(b, bs_f, dt_f){
           boot_data <- 
             select(dt_f, -target) %>% 
             mutate(target=bs_f[, b])
           boot_model <- 
             glm(target~., family="binomial", data=boot_data)
           boot_model$coefficients %>% 
             t() %>% 
             as_tibble() %>% 
             return()
         }, 
         boot_sample_lgr_causal,
         dt_causal)
```

```{r}
ci_lower_lgr_causal <-
  2*lgr_causal$coefficients - apply(boot_est_lgr_causal, 2, quantile, 0.975)

ci_upper_lgr_causal <-
  2*lgr_causal$coefficients - apply(boot_est_lgr_causal, 2, quantile, 0.025)

lgr_causal_coef <- 
  tibble(var=names(lgr_causal$coefficients),
         lower=ci_lower_lgr_causal,
         est=lgr_causal$coefficients,
         upper=ci_upper_lgr_causal) %>% 
  mutate(var=str_replace(var, "([a-z]+)([A-Z])", "\\1: \\2"))

ggplot() +
  geom_point(data=lgr_causal_coef, aes(x=var, y=est)) + 
  geom_segment(data=lgr_causal_coef, aes(x=var, xend=var, y=lower, yend=upper)) + 
  geom_hline(yintercept=0, color="red", linetype="dashed") +
  coord_flip() + 
  labs(title="Logistic Regression Coefficient Estimates",
       subtitle="Bootstrap 95% Confidence Intervals",
       x="Variable",
       y="Coeffient Estimate (log-odds)") 
```

```{r}
set.seed(342)

lasso_causal_pv <- 
  model.matrix(target~., data=dt_causal_scaled)[, -1]

cv_lasso_causal <- 
  cv.glmnet(y=dt$target %>% as.numeric(),
            x=lasso_causal_pv,
            alpha=1, family="binomial") 

lasso_causal_mdl <- 
  glmnet(y=dt$target %>% as.numeric(), 
         x=lasso_causal_pv,
         alpha=1, family="binomial", 
         lambda=cv_lasso_causal$lambda.1se)

lasso_causal_mdl_fit_prob <- 
  predict.glmnet(lasso_causal_mdl, newx=lasso_causal_pv) %>% 
  magrittr::extract(,1) %>% 
  odds_to_prob()
```

```{r}
set.seed(333)

boot_sample_lasso_causal <- 
  replicate(1000, 
            rbernoulli(nrow(dt), 
                       p=lasso_causal_mdl_fit_prob) %>% as.numeric())

boot_est_lasso_causal <- 
  map_df(1:1000, 
         function(b, bs_f, dt_f){
           boot_data <- 
             select(dt_f, -target) %>% 
             mutate(target=bs_f[, b]) %>% 
             model.matrix(target~., data=.) %>% 
             magrittr::extract(, -1)
           boot_model <- 
             glmnet(y=bs_f[, b], x=boot_data,
                    alpha=1, family="binomial", lambda=cv_lasso_causal$lambda.1se)
           boot_model$beta %>%
             as.matrix() %>% 
             t() %>% 
             as_tibble() %>% 
             return()
         }, 
         boot_sample_lasso_causal,
         dt_causal)
```

```{r}
lasso_causal_coef <- magrittr::extract(lasso_causal_mdl$beta, , 1)

ci_lower_lasso_causal <- 
  2*lasso_causal_coef - apply(boot_est_lasso_causal, 2, quantile, 0.975)

ci_upper_lasso_causal <-
  2*lasso_causal_coef - apply(boot_est_lasso_causal, 2, quantile, 0.025)

lasso_causal_tib <- 
  tibble(var=names(lasso_causal_coef),
         lower=ci_lower_lasso_causal,
         est=lasso_causal_coef, 
         upper=ci_upper_lasso_causal) %>% 
  mutate(var=str_replace(var, "([a-z]+)([A-Z])", "\\1: \\2"))

lasso_causal_coef_plot <- 
  ggplot() +
  geom_point(data=lasso_causal_tib, aes(x=var, y=est)) + 
  geom_segment(data=lasso_causal_tib, aes(x=var, xend=var, y=lower, yend=upper)) + 
  geom_hline(yintercept=0, color="red", linetype="dashed") +
  coord_flip() + 
  labs(title="Logistic Lasso Coefficient Estimates",
       subtitle="Bootstrap 95% Confidence Intervals",
       x="Variable",
       y="Coeffient Estimate (log-odds)") 

lasso_causal_coef_plot
```

We can visualize the treatment effects on the probability scale.

```{r, fig.height=12}
map(2:8, function(n){
  treatment_plotting <- 
    tibble(odds=seq(from=-4, to=4, by=0.1),
           prob=odds_to_prob(odds),
           lower=odds_to_prob(odds + ci_lower_lgr_causal[n]),
           mean=odds_to_prob(odds +  lgr_causal_coef$est[n]),
           upper=odds_to_prob(odds + ci_upper_lgr_causal[n]))
  ggplot() + 
    geom_line(data=treatment_plotting, mapping=aes(x=prob, y=prob),
              color="red", linetype="dashed") + 
    geom_line(data=treatment_plotting, mapping=aes(x=prob, y=mean)) + 
    geom_ribbon(data=treatment_plotting, mapping=aes(x=prob, ymin=lower, ymax=upper),
                alpha=0.5) + 
    labs(title=names(ci_lower_lgr_causal)[n],
         x=NULL,
         y=NULL)
}) %>% 
  grid.arrange(grobs=., ncol=2, 
               top="Logistic Regression Estimated Treatment Effects")

map(1:7, function(n){
  treatment_plotting <- 
    tibble(odds=seq(from=-4, to=4, by=0.1),
           prob=odds_to_prob(odds),
           lower=odds_to_prob(odds + ci_lower_lasso_causal[n]),
           mean=odds_to_prob(odds + lasso_causal_coef[n]),
           upper=odds_to_prob(odds + ci_upper_lasso_causal[n]))
  ggplot() + 
    geom_line(data=treatment_plotting, mapping=aes(x=prob, y=prob),
              color="red", linetype="dashed") + 
    geom_line(data=treatment_plotting, mapping=aes(x=prob, y=mean)) + 
    geom_ribbon(data=treatment_plotting, mapping=aes(x=prob, ymin=lower, ymax=upper),
                alpha=0.5) + 
    labs(title=names(ci_lower_lasso_causal[n]),
         x=NULL,
         y=NULL)
}) %>% 
  grid.arrange(grobs=., ncol=2,
               top="LASSO Estimated Treatment Effects")
```

## Test Accuracy and Comparison to Machine Learning Models

We use 5-fold cross-validation to estimate test error for our models. 

```{r}
set.seed(75)
dt_folded <- 
  mutate(dt, 
         fold=rep_len(1:5, length.out=nrow(dt)) %>% 
           sample())

dt_causal_folded <- 
  bind_cols(dt_causal, select(dt_folded, fold))
```

```{r}
# 5-fold-cv for logistic causal model
map_df(1:5, 
    function(n_fold, dt_f){
      train_set <- 
        filter(dt_f, fold!=n_fold) %>% 
        select(-fold)
      test_set <- 
        filter(dt_f, fold==n_fold) %>% 
        select(-fold)
      
      trained_model <- glm(target~., family="binomial", 
                           data=train_set)
      predicted_prob <- 
        predict.glm(trained_model, newdata=select(test_set, -target)) %>% 
        odds_to_prob()
      tibble(Predicted=ifelse(predicted_prob>=0.5, 1, 0) %>% 
               as.factor(),
             True=test_set$target) %>% 
        return()
    }, 
    dt_causal_folded) %>% 
  group_by_all() %>% 
  summarise(Count=n()) %>% 
  mutate(Proportion=round(Count/nrow(dt), 2)) %>% 
  kable(caption="Estimated Test Accuracy of Logistic Model") %>% 
  kable_styling(bootstrap_options = c("striped", "condensed"), 
                full_width=FALSE)

# 5-fold-cv for LASSO logistic causal model
map_df(1:5, 
    function(n_fold, dt_f){
      train_set <- 
        filter(dt_f, fold!=n_fold) %>% 
        select(-fold)
      test_set <- 
        filter(dt_f, fold==n_fold) %>% 
        select(-fold)
      
      lasso_train_pv <- 
        model.matrix(target~., data=train_set)[, -1]
      lasso_test_pv <- 
        model.matrix(target~., data=test_set)[, -1]
      
      trained_model <- 
        glmnet(train_set$target,
               x=lasso_train_pv,
               alpha=1,
               family="binomial",
               lambda=cv_lasso_causal$lambda.1se)
        
      predicted_prob <- 
        predict.glmnet(trained_model, newx=lasso_test_pv) %>% 
        magrittr::extract(,1) %>% 
        odds_to_prob()
      
      tibble(Predicted=ifelse(predicted_prob>=0.5, 1, 0) %>% 
               as.factor(),
             True=test_set$target) %>% 
        return()
    }, 
    dt_causal_folded) %>% 
  group_by_all() %>% 
  summarise(Count=n()) %>% 
  mutate(Proportion=round(Count/nrow(dt), 2)) %>% 
  kable(caption="Estimated Test Accuracy of LASSO Logistic Model") %>% 
  kable_styling(bootstrap_options = c("striped", "condensed"), 
                full_width=FALSE)
```

```{r}
map_df(1:5, 
    function(n_fold, dt_f){
      train_set <- 
        filter(dt_f, fold!=n_fold) %>% 
        select(-fold)
      test_set <- 
        filter(dt_f, fold==n_fold) %>% 
        select(-fold)
      
      trained_model <- randomForest(target~., data=train_set)
      
      tibble(Predicted=predict(trained_model, newdata=test_set) %>% 
               as.factor(),
             True=test_set$target) %>% 
        return()
    }, 
    mutate(dt_folded, target=as.factor(target))) %>% 
  group_by_all() %>% 
  summarise(Count=n()) %>% 
  mutate(Proportion=round(Count/nrow(dt), 2)) %>% 
  kable(caption="Estimated Test Accuracy of Random Forest Model") %>% 
  kable_styling(bootstrap_options = c("striped", "condensed"), 
                full_width=FALSE)

map_df(1:5, 
    function(n_fold, dt_f){
      train_set <- 
        filter(dt_f, fold!=n_fold) %>% 
        select(-fold)
      test_set <- 
        filter(dt_f, fold==n_fold) %>% 
        select(-fold)
      
      train_pv <- 
        model.matrix(target~., data=train_set)[, -1]
      test_pv <- 
        model.matrix(target~., data=test_set)[, -1]
      
      trained_model <- 
        xgboost(data=train_pv, label=train_set$target,
                max_depth=6, nrounds=10,
                objective="binary:logistic",
                nthread=8,
                verbose=FALSE)
        
      predicted_prob <- 
        predict(trained_model, test_pv)
      
      tibble(Predicted=ifelse(predicted_prob>=0.5, 1, 0) %>% 
               as.factor(),
             True=test_set$target) %>% 
        return()
    }, 
    dt_folded) %>% 
  group_by_all() %>% 
  summarise(Count=n()) %>% 
  mutate(Proportion=round(Count/nrow(dt), 2)) %>% 
  kable(caption="Estimated Test Accuracy of Gradient Boosting Model") %>% 
  kable_styling(bootstrap_options = c("striped", "condensed"), 
                full_width=FALSE)
```

There is not noticeable improvement in test error when using either random forest or gradient boosting. 

# Discussion

Our conclusions have limited generalizability to a wider population. It appears the sample of individuals in this study were seeking some kind of treatment for chest pain or other cardiovascular issues (we were unable to find the original study or much more context for the data). We saw this reflected in graphical EDA when looking at the demographic makeup of these individuals: two-thirds were men, for example. At best, our results estimate treatment effects for individuals who are already seeking treatment, which implies there are numerous other confounding variables we do not have access to (e.g. income, race, medical history) which were not accounted for when estimating the causal graph. 

For this limited population, we see a substantial increase in probability of heart disease for those who show a "fixed defect" (e.g. scarring or other permanent damage) compared to a normal result when taking a thallium stress test. Fluoroscopy results indicate substantially increased probability of heart disease for each decrease in blood vessel colored by the test. Fluoroscopy is used to analyze blood flow^[https://medical-dictionary.thefreedictionary.com/angiography], so fewer colored blood vessels would likely indicate some kind of obstruction. Finally, we see a substantial decrease in probability of heart disease in those who have exercise-induced angina. It is possible individuals with this issue sought treatment because of it, but it was not related to heart disease. More data would be needed to understand these relationships